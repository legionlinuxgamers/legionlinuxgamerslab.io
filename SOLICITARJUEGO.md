# Solicita tu juego

Nuestra comunidad se encuentra abierta a las solicitudes de todos los usuarios, las mismas se analizan en cuestión de posibilidad/uso para la creación de servidores.

## ¿Puedo unir mis servidores?

Si, podemos agregar tus servidores a la comunidad, sin embargo para poder formar parte de la comunidad los servidores deben estar identificado como parte de LLG ya sea poniendole este nombre al servidor o [LLG] al inicio (Esto depende el juego se charla en issue)
También, para poder ingresar servidores es necesario tener una comunidad registrada en LLG o conseguir que una comunidad ofrezca dar soporte para este juego.

## ¿Cómo solicitar un servidor nuevo?

Para poder solicitar un servidor nuevo necesitamos toda la información posible para poder hacer un mejor análisis. Es importante recalcar que los juegos y/o servidores deben estar disponibles para GNU/Linux (Puede ser porteado pero debe ser estable)

La solicitud se debe hacer mediante un Issue en este repositorio con la siguiente información:

* Juego: (ej Wow)
* Tipo de Juego: (ej MMORPG)
* Requisitos mínimos:
* Nativo: (ej No, porteado con Wine)
* Files (si lo tienes): (ej No conozco files)
* Compromiso: (ej Puedo desarrollar el server, puedo poner un host, puedo...)
* Análisis de jugabilidad: (ej Pocos servers estables, pocos jugadores )


## ¿Como Agrego mi servidor a la Comunidad?

Para agregar tu servidor a la comunidad debe cumplir con los requisitos establecidos anteriormente. También debe pedirse mediante un Issue en este repositorio que debe contener la siguiente información.

* Juego: (ej Supertuxkart)
* Tipo de Juego: (ej Race)
* Requisitos mínimos:
* Nativo: (ej Si)
* Descarga de Juego: (ej Link de descarga de stk)
* IP: (ej stk.patojad.com.ar, 182.125.23.59, stkserver.ddns.com) **DEBE SER ESTATICA**
* Nombre del servidor: (ej Legión Linux Gamers STK)
* Comunidad que ofrece soporte: (ej PatoJAD Community)
* Lugar de Hosteo: (ej Argentina dedicado)
* Horarios: (ej 24h/7d)