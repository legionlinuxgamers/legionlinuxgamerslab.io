---
title: "Obtener Mutant Year Zero: Road to Eden FREE"
img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQziE_5q5dFYfdRINlrcdgdblsW6-UPo7ub3Phu8AyHJ64UkGu0"
date: "2021-12-22 13:00:00"
tags: ["epic", "free", "epicgames", "gratis", "Mutant Year Zero","Road to Eden", "mutant year zero", "road to eden", "muntant"]
---

> La humanidad ha sido erradicada por el cambio climático, la guerra nuclear y las pandemias. ¿Podrá sobrevivir tu equipo de mutantes a la Zona? Mutant Year Zero: Road to Eden es una aventura táctica que combina el combate por turnos con el sigilo y la exploración en tiempo real.

Un equipo formado por antiguos diseñadores de HITMAN y el coautor de PAYDAY ha creado Mutant Year Zero: Road to Eden, una aventura estratégica que combina el combate por turnos de XCOM con sigilo y exploración en tiempo real en un mundo posthumano tomado por la naturaleza y... por mutantes.

Por supuesto, el fin del mundo ha llegado.

Ahora, todo ha acabado, pero la Tierra sigue en pie. La naturaleza ha invadido las ciudades en ruinas. y el viento corre entre las calles vacías, que ahora no son más que cementerios.

Los humanos han desaparecido. y en la actualidad, la Tierra está plagada de mutantes, una mezcla entre humanoides deformes y animales, que rebuscan entre los restos de la civilización con la esperanza de encontrar la salvación o al menos algo que llevarse a la boca. Para sobrevivir, tus compañeros y tú debéis aventuraros en la Zona.

O puede que no, quizá no sean más que tonterías.


{{< img src="https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcRlgW6TJBQQv_mgH94H1SIWDX31yCOwB2RkwRxdnB2TeyXKp0k2" >}}

#### COMBATE ESTRATÉGICO

Mutant Year Zero: Road to Eden es justo lo que necesitas para calmar tu adicción a los juegos estratégicos. Sumérgete en un sistema de combate táctico, profundo y por turnos inspirado en XCOM.
EXPLORA UNA TIERRA TRAS LA EXTINCIÓN DE LA HUMANIDAD
Aventúrate en un mundo posthumano repleto de ciudades abandonadas, autopistas derruidas y con una vegetación totalmente descontrolada. Reabastécete y planea tu siguiente aventura en el Arca, un oasis de neones lleno de personajes de mala reputación y moral cuestionable.

#### CONTROLA UN EQUIPO DE MUTANTES

Por ejemplo, a un pato con problemas de comportamiento o un jabalí que no sabe controlar su ira... Y es que aquí no encontrarás a los típicos héroes. Conoce a una gran variedad de personajes, como Dux, Bormin o Selma, y descubre la característica personalidad y retorcida visión que tiene cada uno de ellos sobre el mundo y la situación en la que se encuentran.

#### DOMINA EL SIGILO

Camina entre las sombras para evitar los conflictos y pilla a los enemigos desprevenidos. El sigilo en tiempo real te permite tener un total control de tu estrategia: adéntrate en el campamento enemigo sin llamar la atención, posiciona a tu equipo de mutantes como mejor te convenga y aprovéchate del factor sorpresa.

#### DESBLOQUEA MUTACIONES

Consigue nuevas mutaciones y habilidades para tus mutantes, como la piel pétrea de Selma, la carga de Bormin y la asombrosa habilidad de Dux para adentrarse en un campamento lleno de enemigos sin que lo detecten, a pesar de ser un pato de más de un metro de altura que camina, habla y va con una ballesta en las manos.

#### ENTORNOS DINÁMICOS

Aprovéchate del entorno; aléjate de los focos, ocúltate de la línea de visión enemiga o derriba paredes y edificios destruibles para sembrar el caos.

#### LOOT, LOOT EVERYWHERE

Encuentra de todo, desde tirachinas improvisados o fusiles de gran potencia hasta chisteras o chalecos antibalas, y equipa como es debido a tus mutantes para que puedan hacer frente a cualquier peligro. Nada podría representar mejor la era de la posthumanidad que un jabalí mutante pertrechado con una armadura de pinchos de metal yendo hacia ti con un trabuco en ristre.

### Requisitos

* Intel Core i5-760 / AMD Phenom II X4 965
* 6 GB DE RAM
* 8 GB de espacio disponible
* Nvidia GTX 580 / AMD Radeon HD 7870

{{< link url="https://www.epicgames.com/store/es-ES/p/mutant-year-zero" text="Obtener Gratis" >}}
