---
title: "Obtener Loop Hero FREE"
img: "https://cdn1.epicgames.com/ff50f85ed609454e80ac46d9496da34d/offer/EGS_TheLichhasthrowntheworldintoatimelessloop_FourQuarters_S1-2560x1440-dd971b1a2d79b2984c22a3b7c66c60da.jpg"
date: "2021-12-20 13:00:00"
tags: ["epic", "free", "epicgames", "gratis", "Loop Hero", "loop hero", "hero", "loop"]
---

> El Lich ha enviado al mundo entero a un bucle atemporal y ha sumido a sus habitantes en un interminable caos. Utiliza un mazo de cartas místicas en constante expansión para colocar enemigos, edificios y terrenos en cada bucle único de expedición a disposición del valiente héroe.

El Lich ha enviado al mundo entero a un bucle atemporal y ha sumido a sus habitantes en un interminable caos. Utiliza un mazo de cartas místicas en constante expansión para colocar enemigos, edificios y terrenos en cada bucle único de expedición a disposición del valiente héroe. Consigue poderosos objetos y equipa a cada clase de héroe en cada batalla. Amplía el campamento de los supervivientes para reforzar cada aventura en el bucle. Desbloquea nuevas clases, nuevas cartas y taimados guardianes que te ayudarán a destruir el eterno ciclo de desesperación.


{{< img src="https://cdn.akamai.steamstatic.com/steam/apps/1282730/ss_535cb6f102a0a421f6b50ccb509e10074067bb89.1920x1080.jpg?t=1633530897" >}}

#### Una aventura infinita:

Elige un personaje de entre las diferentes clases y las cartas del mazo desbloqueables antes de embarcarte en cada expedición a lo largo de un bucle generado de forma aleatoria. No hay dos expediciones iguales.

#### Planifica tu batalla:

Coloca las cartas de edificios, terrenos y enemigos de forma estratégica en cada bucle para crear tu propia ruta peligrosa. Encuentra el equilibrio entre las cartas para aumentar tus probabilidades de supervivencia a medida que obtienes botines y recursos valiosos para tu campamento.

#### Saquea y mejora:

Acaba con criaturas temibles, consigue objetos más poderosos para equiparlos sobre la marcha y desbloquea nuevas ventajas por el camino.

#### Amplía tu campamento:

Mejora tu campamento con los recursos que tanto te ha costado conseguir y obtén valiosos refuerzos con cada bucle completado en la ruta de expedición.

#### Salva el mundo perdido:

Vence a los infames jefes guardianes en una gran saga para salvar el mundo e destruir el bucle temporal del Lich.

### Requisitos

* ntel Core2 Duo E4500 (2 * 2200) o equivalente, AMD Athlon 64 X2 Dual Core 3600+ (2 * 1910) o equivalente
* 2 GB
* 200 MB
* GeForce 7300 GT (512 MB), Radeon X1300 Pro (256 MB)

{{< link url="https://www.epicgames.com/store/es-ES/p/loop-hero" text="Obtener Gratis" >}}
