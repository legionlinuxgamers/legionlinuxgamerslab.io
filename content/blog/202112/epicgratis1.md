---
title: "Obtener Godfall Challenger Edition FREE"
img: "https://cdn1.epicgames.com/offer/b6b2ef0cc19a4adaaa1cf6c7ed000dfa/EGS_GodfallChallengerEdition_CounterplayGames_Editions_S1_2560x1440-2541e6e7783e8a65817c51c0ad856c19"
date: "2021-12-15 17:10:00"
tags: ["epic", "free", "epicgames", "gratis", "godfall", "challenger edition"]
---

> ¡Desata el poder de Godfall al instante! Godfall Challenger Edition desbloquea de forma inmediata un Valorplate de nivel máximo, te regala puntos de habilidad y te equipa con una variedad de armas letales. ¡Enfréntate a los tres modos de juego finales!

¡Desata el poder de Godfall al instante! Godfall Challenger Edition desbloquea de forma inmediata un Valorplate de nivel máximo, te regala puntos de habilidad y te equipa con una variedad de armas letales. Enfréntate a los tres modos de juego finales: Lightbringer, Piedras de los Sueños y Torre de las Pruebas Ascendida. Derrota a tus enemigos y se te recompensará con un botín digno de un auténtico valoriano. Saquea y corta en el modo cooperativo emparejado con hasta 3 jugadores para demostrar tus habilidades, mejorar a tu personaje y aplastar a tus enemigos.

{{< img src="https://cdn2.unrealengine.com/godfall-challenger-edition-equipment-1920x1080-cde97d87423a.png" >}}

* Desbloquea al instante el poder de un Valorplate de nivel máximo
* Saquea equipo y armas devastadoras
* Combina diferentes habilidades con un poderoso equipo para mejorar a tu personaje
* Desbloquea al instante los tres modos de juego finales
* Domina cinco clases de armas únicas
* Desafía a jefes legendarios
* Usa la forja para mejorar y encantar armas
* Enfréntate a misiones épicas y consigue recompensas
* Todas las ediciones de Godfall son compatibles con el modo cooperativo y el emparejamiento

### Requisitos

* Intel Core i5-6600 | AMD Ryzen 5 1600
* 12 GB de RAM
* 50 GB (SSD recomendada)
* Nvidia GeForce GTX 1060, 6 GB | AMD Radeon RX 580, 8 GB

{{< link url="https://www.epicgames.com/store/es-ES/p/godfall--challenger-edition" text="Obtener Gratis" >}}
