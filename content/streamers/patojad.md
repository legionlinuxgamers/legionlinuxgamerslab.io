---
title: "PatoJAD"
description: "Linuxtuber"
img: "https://i.postimg.cc/K8KDZ0v5/image.png"
youtube: "https://www.youtube.com/c/PatoJAD"
twitch: "https://www.twitch.tv/patojad"
instagram: "https://www.instagram.com/patojadoficial/"
mastodon: "https://mastodon.social/@PatoJAD"
twitter: "https://twitter.com/PatoJADOficial"
facebook: "https://www.facebook.com/PatoJAD/"
steam: "https://steamcommunity.com/id/joaquindecima/"
web: "https://patojad.com.ar/"
telegram: "https://t.me/PatoJAD"
nacionalidad: "Argentina"
channel: "patojad"
data: "Pato es el fundador de PatoJAD una comunidad que rapidamente se volvio mucho mas grande lo que imaginaba. Está iniciando como un pequeño streamer que busca mantener la alegría de jugar con usuarios, hablar con ellos y mantener una relación muy alegre con su comunidad. Utiliza los streams para conectar con las personas de una forma más funcional que un video de youtube."
---

{{< streamers/streamersdata >}}
