---
title: "Jorgicio"
description: "Streamer"
img: "https://i.postimg.cc/QdRpfwmC/image.png"
twitch: "https://twitch.tv/jorgicio"
instagram: "https://instagram.com/jorgicio"
steam: "https://steamcommunity.com/id/jorgicio/"
telegram: "https://t.me/Jorgicio"
nacionalidad: "Chile"
channel: "jorgicio"
data: "Jorgicio es un miembro activo de Legion Linux Gamers y un streamer ocasional producto de la pandemia del COVID-19. Suele hacer streams los fines de semana, donde está más activo en Twitch para entretener e interactuar con su comunidad."
---

{{< streamers/streamersdata >}}
