---
title: "Fistful of Frags [LiNuXiToS]"
date: "2022-06-08 18:10:00"
tags: ["Fistful of Frags", "shooter", "servidor", "2", "fistful", "of", "frags", "cowboy"]
category: ["Servers"]
img: "/img/games/fofbanner.webp"
logo: "/img/games/foflogo.png"
ip: "linuxitos.ddnsking.com"
port: 27910
code: "barotrauma"
onlinetime: "24/7"
nointro: false
resumen: "Juego de disparos basado en multijugador, en primera persona, lleno de acción y que exige habilidades, ambientado en los tiempos del Lejano Oeste. Una combinación única de acción continua y armamento lento pero poderoso."
requisitos: ["Ubuntu 12.04", "Dual core from Intel or AMD at 2.8 GHz", "2 GB de RAM", "Conexión de banda ancha a Internet", "4 GB de espacio disponible", "nVidia GeForce 8 series or better, ATI/AMD Radeaon HD2600/3600 (Graphic Drivers: nVidia 310, AMD 12.11), OpenGL 2.1"]
---

{{< server/servernodata >}}
