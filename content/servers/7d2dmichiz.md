---
title: "7 Days to Die [MichiZ]"
date: "2021-02-14 08:10:00"
tags: ["7 days 2 die", "7d2d", "7days", "servidor", "zombies", "server 7d2d"]
category: ["Servers"]
img: "/img/games/7d2dbanner.webp"
logo: "/img/games/7d2dlogo.png"
ip: "192.168.0.92.mooo.com"
port: 26900
code: "7d2d"
onlinetime: "24/7"
nointro: false
resumen: "7 Days to Die es un juego de mundo abierto que es una combinación única de disparos en primera persona, survival horror, defensa de torres y juegos de rol. Juega el juego de rol de sandbox de supervivencia zombi definitivo que llegó primero. ¡Navezgane espera!"
requisitos: ["Ubuntu 16.04 (64-bit)", "2.4 Ghz Dual Core CPU", "8 GB de RAM", "2 GB de VRAM", "Conexión de banda ancha a Internet", "12 GB de espacio disponible", "OpenAL Compatible Sound Card"]
---

{{< server/serverdata >}}
{{< server/serverjs >}}
