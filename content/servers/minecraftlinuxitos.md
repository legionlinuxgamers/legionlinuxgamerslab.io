---
title: "Minecraft [LiNuXiToS]"
date: "2020-06-04 08:10:00"
tags: ["minecraft", "1.2", "craft", "servidor", "mine", "server minecraft"]
category: ["Servers"]
img: "/img/games/minecraftbanner.webp"
logo: "/img/games/minecraftlogo.png"
ip: "linuxitos.ddnsking.com"
port: 25565
code: "minecraft"
onlinetime: "24/7"
nointro: false
resumen: "Minecraft es un videojuego de construcción, de tipo «mundo abierto» o sandbox creado originalmente por el sueco Markus Persson (conocido comúnmente como Notch),​ y posteriormente desarrollado por su empresa, Mojang Studios."
requisitos: ["Linux Ubuntu 12.04", "Intel Core i3-3210 3.2 GHz/ AMD A8-7600 APU 3.1 GHz", "4GB RAM", "Intel HD Graphics 4000 (Ivy Bridge) o serie AMD Radeon R5 (Kaveri line)", "Conexión de banda ancha a Internet", "1GB Disco Duro", "OpenGL 4.4"]
---

{{< server/serverdata >}}
{{< server/serverjs >}}
