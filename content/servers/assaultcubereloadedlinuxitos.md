---
title: "Assault Cube Reloaded [LiNuXiToS]"
date: "2020-07-17 08:10:00"
tags: ["Assault Cube Reloaded", "shooter", "servidor", "assault", "cube", "reloaded"]
category: ["Servers"]
img: "/img/games/acrbanner.webp"
logo: "/img/games/acrlogo.png"
ip: "linuxitos.ddnsking.com"
port: 28770
code: "quake1"
onlinetime: "24/7"
nointro: false
resumen: "AssaultCube es un videojuego de acción en primera persona basado en el motor Cube. AssaultCube está hecho para jugar en línea por servidores, pero tiene un modo offline en el que se puede jugar con bots, emulando una partida en línea."
requisitos: ["Pentium 266 Mhz", "64 Mb de RAM", "Tarjeta 3D, 32 MB", "Conexión de banda ancha a Internet", "600 MB de espacio disponible", "OpenGL 2.0 with GLSL 1.20"]
---

{{< server/servernodata >}}
