---
title: "Counter-Strike GO [PatoJAD]"
date: "2020-06-07 08:10:00"
tags: ["counter", "cs", "go", "servidor", "global offecive", "server cs"]
category: ["Servers"]
img: "/img/games/csgobanner.webp"
logo: "/img/games/csgologo.png"
ip: "patojad.mooo.com"
port: 27015
code: "csgo"
onlinetime: "24/7"
nointro: false
resumen: "Counter-Strike cogió la industria de los videojuegos por sorpresa cuando, contra todo pronóstico, el MOD se convirtió en el juego de acción online para PC más jugado del mundo tras su lanzamiento en agosto de 1999."
requisitos: ["Ubuntu 12.04", "64-bit Dual core from Intel or AMD at 2.8 GHz", "4 GB de RAM", "nVidia GeForce 8600/9600GT, ATI/AMD Radeon HD2600/3600 (Graphic Drivers: nVidia 310, AMD 12.11), OpenGL 2.1", "Conexión de banda ancha a Internet", "15 GB de espacio disponible", "OpenAL Compatible Sound Card"]
---

{{< server/serverdata >}}
{{< server/serverjs >}}
