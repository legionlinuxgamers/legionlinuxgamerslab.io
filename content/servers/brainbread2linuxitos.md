---
title: "BrainBread 2 [LiNuXiToS]"
date: "2022-06-07 18:10:00"
tags: ["BrainBread 2", "shooter", "servidor", "2", "brainbread", "zombie"]
category: ["Servers"]
img: "/img/games/bb2banner.webp"
logo: "/img/games/bb2logo.png"
ip: "linuxitos.ddnsking.com"
port: 28772
code: "barotrauma"
onlinetime: "24/7"
nointro: false
resumen: "Coge un arma, derriba a tus enemigos, sube de nivel, sé más poderoso, deja que la sangre fluya, deja que las extremidades vuelen. BrainBread 2 presenta un fps zombie mezclado con elementos RPG / Arcade, el juego está lleno de acción y, en general, es rápido."
requisitos: ["Ubuntu 14.x, CentOS 7.x", "1.7 GHz processor or equivalent", "512 MB de RAM", "Conexión de banda ancha a Internet", "6 GB de espacio disponible", "ATI Radeon 9600 or Nvidia GeForce 500 series"]
---

{{< server/servernodata >}}
