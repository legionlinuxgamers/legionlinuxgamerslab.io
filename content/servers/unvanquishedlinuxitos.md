---
title: "Unvanquished [LiNuXiToS]"
date: "2020-06-12 08:10:00"
tags: ["Unvanquished", "linuxitos", "unvanquished", "servidor", "shooter", "server Unvanquished"]
category: ["Servers"]
img: "/img/games/unvanquished.webp"
logo: "/img/games/unvanquished.png"
ip: "linuxitos.ddnsking.com"
port: 27960
code: "quake3"
onlinetime: "OnDemand"
nointro: false
resumen: "¡juega y salva a tu especie de la invasión! Conviértete en un marine, maneja tu gran arma y viste ese impresionante exoesqueleto para salvar a la raza humana como un jefe. O si lo prefieres, sé una bestia monstruosa y salva a esos adorables granjeros de esos invasores de dos patas que solo saben cómo convertir el hermoso equilibrio orgánico en máquinas sin vida y mundos de hormigón muerto."
requisitos: ["Ubuntu 12.04 or newer", "Pentium II 233 MHz o AMD 350 MHz K6-2 o Athlon", "64 MB RAM", "Tarjeta de vídeo: 8 MB", "427 MB de espacio disponible", "Placa de sonido compatible con OpenAL"]
---

{{< server/serverdata >}}
{{< server/serverjs >}}
