let localStorage = window.localStorage;
const root = document.documentElement;
let isDark = false || localStorage.getItem('isDark');
const themeButton = document.getElementById("theme-changer");

updateTheme();

function updateTheme(){
  if (isDark) {
    root.style.setProperty('--background' , '#333');
    root.style.setProperty('--background-2', "#111");
    root.style.setProperty('--border', "#222");
    root.style.setProperty('--text-color', "#FFF");
    root.style.setProperty('--text-color-2', "#BBB");
    themeButton.innerHTML = '<i class="fa fa-sun" aria-hidden="true"></i>';
  } else {
    root.style.setProperty('--background' , '#FFF');
    root.style.setProperty('--background-2', "#F6F9FC");
    root.style.setProperty('--border', "#eee");
    root.style.setProperty('--text-color', "#081828");
    root.style.setProperty('--text-color-2', "#7E8890");
    themeButton.innerHTML = '<i class="fa fa-moon" aria-hidden="true"></i>';
  }
}

function changeTheme(){
  isDark = !isDark;
  localStorage.setItem('isDark', isDark);
  updateTheme();
}
